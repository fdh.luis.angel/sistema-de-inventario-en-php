# Sistema de inventario en PHP

## Run project
php artisan serve


@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <p>Welcome to this beautiful admin panel.</p>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop

https://github.com/jeroennoten/Laravel-AdminLTE/wiki/Menu-Configuration

https://www.larablocks.com/package/jeroennoten/laravel-adminlte


Rename .env.example file to .env inside your project root and fill the database information. (windows won't let you do it, so you have to open your console cd your project root directory and run mv .env.example .env )

Run composer install
Run php artisan key:generate


