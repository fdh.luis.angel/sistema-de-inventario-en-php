<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/login', function () {
    return '<h1>Login</h1>';
});

Route::get('/logout', function () {
    return "<h1>Logout</h1>";
});

//---------------------------------------------------- USUARIOS
Route::get('admin/users', function () {
    return view('users.users');
});

Route::post('admin/users', function () {
    return "Añadiendo usuario";
});

Route::get('admin/users/{id}', function ($id) {
    return "<h1>Vista detalles usuario: ".$id."</h1>";
})
->where('id','[0-9]+');

//---------------------------------------------------- PRODUCTOS
Route::get('admin/products', function () {
    return view('products.products');
});


Route::post('admin/products', function () {
    return "Añadiendo productos";
});


Route::get('admin/products/{id}', function ($id) {
    return "<h1>Vista detalles producto: ".$id."</h1>";
})
->where('id','[0-9]+');

//---------------------------------------------------- LABORATORIOS
Route::get('admin/labs', function () {
    return view('labs.labs');
});

Route::post('admin/labs', function () {
    return "Añadiendo laboratorios";
});

Route::get('admin/labs/{id}', function ($id) {
    return "<h1>Vista detalles laboratorio: ".$id."</h1>";
})
->where('id','[0-9]+');

//---------------------------------------------------- GRUPOS
Route::get('admin/groups', function () {
    return view('groups.groups');
});

Route::post('admin/groups', function () {
    return "Añadiendo grupos";
});

Route::get('admin/groups/{id}', function ($id) {
    return "<h1>Vista detalles grupo: ".$id."</h1>";
})
->where('id','[0-9]+');
//---------------------------------------------------- CLIENTES
Route::get('admin/clients', function () {
    return view('clients.clients');
});


Route::post('admin/clients', function () {
    return "Añadiendo grupos";
});

Route::get('admin/clients/{id}', function ($id) {
    return "<h1>Vista detalles cliente: ".$id."</h1>";
})
->where('id','[0-9]+');

//---------------------------------------------------- PROVEEDORES
Route::get('admin/providers', function () {
    return view('providers.providers');
});

Route::post('admin/providers', function () {
    return "Añadiendo proveedores";
});

Route::get('admin/providers/{id}', function ($id) {
    return "<h1>Vista detalles proveedores: ".$id."</h1>";
})
->where('id','[0-9]+');

//---------------------------------------------------- FACTURAS
Route::get('admin/invoices', function () {
    return view('invoices.invoices');
});

Route::post('admin/invoices', function () {
    return "Añadiendo facturas";
});

Route::get('admin/invoices/{id}', function ($id) {
    return "<h1>Vista detalles facturas: ".$id."</h1>";
})
->where('id','[0-9]+');


//---------------------------------------------------- PROVEEDORES
Route::get('admin/backups', function () {
    return view('backups.backups');
});

Route::post('admin/backups', function () {
    return "Subiendo backups";
});

//---------------------------------------------------- VENTAS
Route::get('admin/sales', function () {
    return view('sales.sales');
});


//---------------------------------------------------- TOKEN
Route::get('api/token', function () {
    return csrf_token(); 
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
