<style>
    .side-menu {
        z-index: 100;
        background-color: #222d32;
        width: 77px;
        height: 100vh;
        position: fixed;
        top: 0px;
        left: 0px;
        filter: drop-shadow(4px 0px 5px rgba(0, 0, 0, 0.25));
        color: #fff;
        transition: 150ms;
        user-select: none;
    }

    .side-menu a {
        text-decoration: none;
        color: #fff;
    }

    .side-menu:hover {
        width: 300px;
    }

    .side-menu:hover .header-menu .item-menu .item-menu-label,
    .side-menu:hover .body-menu .item-menu .item-menu-label {
        opacity: 1;
        display: inline-block;
        transition-delay: 150ms;
    }

    .side-menu .item-menu {
        height: 60px;
        max-height: 60px;
        padding-left: 15px;
        display: flex;
        align-items: center;
        position: relative;
        cursor: pointer;
    }

    .side-menu .item-menu-label {
        transition-delay: 100ms;
        transition: 150ms;
        opacity: 1;
        width: 200px;
        min-width: 200px;
        margin-left: 20px;
        font-weight: 100;
        display: none;
    }

    .side-menu .item-menu-icon {
        transition: 150ms;
        width: 37px;
        min-width: 37px;
        margin-left: 4px;
    }

    .side-menu .header-menu {
        margin-bottom: 80px;
        background-color: #367FA9;
        height: 80px;
    }

    .side-menu .header-menu .item-menu-label {
        font-weight: 400;
        margin-right: 5px;
    }

    .side-menu .body-menu .item-menu:hover {
        background-color: #2f393d;
    }

    .side-menu .body-menu .item-menu-active,
    .side-menu .body-menu .item-menu:active {
        background-color: #303b40;
    }

    .side-menu .body-menu .item-menu-active .item-menu-indicator,
    .side-menu .body-menu .item-menu:active .item-menu-indicator {
        background-color: #82a53d;
        width: 6px;
    }

    .side-menu .body-menu .item-menu-indicator {
        transition: 250ms;
        width: 0px;
        height: 100%;
        position: absolute;
        right: 0px;
        top: 0px;
    }

    .side-menu .body-menu .avatar-area {
        background-color: #367FA9;
        height: 80px;
    }
</style>

<div class="side-menu">
    <div class="header-menu">
        <div class="item-menu">
            <div class="item-menu-label">
                DROGUERIA SANAR
            </div>
        </div>
    </div>

    <div class="avatar-area ">
        AVATAR AREA
    </div>

    <div class="body-menu">
        <a href="/" class="item-menu" id="/">
            <div class="item-menu-label">
                Inicio
            </div>
        </a>

        <a href="/providers" class="item-menu" id="/providers">
            <div class="item-menu-label">
                Proveedores
            </div>
        </a>

        <a href="/clients" class="item-menu" id="/clients">
            <div class="item-menu-label">
                Clientes
            </div>
        </a>

        <a href="/products" class="item-menu" id="/products">
            <div class="item-menu-label">
                Productos
            </div>
        </a>

        <div class="item-menu">
            <div class="item-menu-label">
                Copias de seguridad
            </div>
        </div>

        <div class="item-menu">
            <div class="item-menu-label">
                Ventas
            </div>
        </div>
    </div>
</div>

<script>
    const items = document.getElementsByClassName("item-menu");

    for (let i = 0; i < items.length; i++) {
        const item = items[i];
        item.classList.remove("item-menu-active");
    }

    const active = document.getElementById(window.location.pathname.toString());

    if( active ) active.classList.add('item-menu-active');

</script>