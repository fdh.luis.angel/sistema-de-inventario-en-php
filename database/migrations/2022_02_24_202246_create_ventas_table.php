<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->increments('codigo');

            $table->integer('cod_persona')->unsigned();
            $table->foreign('cod_persona')->references('codigo')->on('personas');

            $table->integer('cod_usuario')->unsigned();
            $table->foreign('cod_usuario')->references('codigo')->on('usuarios');
            
            $table->string('num_comprobante');
            $table->date('fecha');
            $table->integer('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
