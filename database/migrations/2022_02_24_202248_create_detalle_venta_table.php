<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleVentaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_venta', function (Blueprint $table) {
            $table->increments('codigo');

            $table->integer('cod_venta')->unsigned();
            $table->foreign('cod_venta')->references('codigo')->on('ventas');

            $table->integer('cod_producto')->unsigned();
            $table->foreign('cod_producto')->references('codigo')->on('productos');

            $table->integer('cantidad');
            $table->integer('precio');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_venta');
    }
}
