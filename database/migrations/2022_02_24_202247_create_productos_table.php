<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('codigo');
            $table->string('nombre');
            $table->integer('precio_venta');
            $table->integer('precio_compra');
            $table->string('descripcion');
            $table->integer('codigo_barras');

            $table->integer('cod_grupo')->unsigned();
            $table->foreign('cod_grupo')->references('codigo')->on('grupos');

            $table->integer('cod_laboratorio')->unsigned();
            $table->foreign('cod_laboratorio')->references('codigo')->on('laboratorios');

            $table->integer('cod_lote')->unsigned();
            $table->foreign('cod_lote')->references('codigo')->on('lote');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
